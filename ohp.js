
function select_tab()
{
    var items = document.getElementsByClassName('selected');
    for(var i = 0; i < items.length; i++) {
	var s = items[i].className.search('selected');
	items[i].className = items[i].className.substr(0,s) + 
	    items[i].className.substr(s+9);
    }
    this.className += ' selected';
    var id=this.title.substr(5);
    var type=this.title.substr(0,4);
    var cName=type+'frame';
    var tabs = document.getElementsByClassName(cName);
    for(var i = 0; i < tabs.length; i++) {
         if (tabs[i].title == id) {
           tabs[i].style.display="inline";
	   if (type=='plot') {
	      images=tabs[i].getElementsByTagName('img');
	      for(var j = 0; j < images.length; j++ ) {
	        images[j].src=images[j].title;
              }
           } else {
	      var items = tabs[i].getElementsByTagName('li');
      	      for(var j = 0; j < items.length; j++) {
		items[j].onclick = select_tab;
      	      }
           }
	 } else {
           tabs[i].style.display="none";
	   if (type=='plot') {
	      images=tabs[i].getElementsByTagName('img');
	      for(var j = 0; j < images.length; j++ ) {
	        images[j].src="";
              }
           }
         }
    }
}

function init_tab(name)
{
    if(name == undefined) name = 'menu';
    var menu = document.getElementsByClassName(name);
    for(var m = 0; m < menu.length; m++)  {
      var items = menu[m].getElementsByTagName('li');
      for(var i = 0; i < items.length; i++) {
	items[i].onclick = select_tab;
      }
    }
}


var root_url = '/info/current/';
var base_url = root_url + 'ATLAS'

function show_state(response)
{
    var s = webis.get_attribute(response.responseXML, 'state') || 'ABSENT';
    document.getElementById('status').innerHTML = s;
    document.getElementById('status').className = s;
}

function show_error(response)
{
  document.getElementById('status').innerHTML = 'ABSENT';
  document.getElementById('status').className = 'ABSENT';
}

function get_status(partition)
{
    partition = partition || 'ATLAS';
    webis.request(root_url + partition + '/is/RunCtrl/RunCtrl.RootController', { type: 'RCStateInfo'}, show_state, show_error);
    setTimeout(get_status, 60000);
}

