#!/bin/bash
BASEDIR=/atlas/moncfg/tdaq-09-03-00
FORCE=no
if [ ""$1 = "force" ]; then
    FORCE=yes
fi

# $1 == input $2 = output
doConvert()
{
    if [ "$FORCE" == "yes" -o $1 -nt ${2}.html ]; then
	python ./ohp2webis.py $1 $2 $3
    fi
}

doConvert ${BASEDIR}/combined/ohp/ohp_Global_Nexus_ATLAS.xml ATLAS  
doConvert ${BASEDIR}/beamspot/ohp/BeamSpot-ohp.data.xml Beamspot  
doConvert ${BASEDIR}/ctp/ohp/bptx-ohp.xml BPTX  
# doConvert ${BASEDIR}/lar/ohp/LArMonitoringExpert.ohp.xml LArExpert
doConvert ${BASEDIR}/lar/ohp/CalMonitoringShifter.ohp.xml CalMonitoringShifter  
doConvert ${BASEDIR}/muons/ohp/muon.ohp.xml MuonShifter
doConvert ${BASEDIR}/muons/ohp/csc/CSCGnam.ohp.xml CSC  
doConvert ${BASEDIR}/muons/ohp/mdt/MDTGnam.ohp.xml MDT  
doConvert ${BASEDIR}/muons/ohp/rpc/RPC.ohp.xml RPC  
doConvert ${BASEDIR}/muons/ohp/tgc/all/TGC-ohp.xml TGC  
doConvert ${BASEDIR}/tile/ohp/Tile.ohp.xml Tile  
#doConvert ${BASEDIR}/trt/ohp/TRTMonitoring.ohp.xml TRT
# doConvert ${BASEDIR}/pixel/ohp/ohp_Nexus_ATLAS.xml Pixel
# doConvert ${BASEDIR}/trigger/ohp/atlas_trigger.ohp_Physics.xml Trigger
doConvert ${BASEDIR}/trigger/ohp/atlas_trigger.ohp_current.xml Trigger
# doConvert ${BASEDIR}/trigger/ohp/atlas_trigger.ohp_Cosmics.xml Trigger
# doConvert ${BASEDIR}/lbcf/ohp/ohp.conf.xml lbcf    
# doConvert ${BASEDIR}/muons/ohp/rpc/RPC.ohp.xml part-RPC part-RPC
#doConvert ${BASEDIR}/sct/ohp/SCT-OHP.xml SCT
doConvert ./DAQ_HLT.xml DAQ
doConvert ${BASEDIR}/indet/ohp/config.xml ID
# doConvert ${BASEDIR}/combined/ohp/timing.xml Timing
doConvert ${BASEDIR}/muons/ohp/rpc/RPC.ohp.xml RPC-DAQSlice part_RPC-DAQSlice
doConvert ${BASEDIR}/zdc/ZdcMonitoring.ohp.xml ZDC
