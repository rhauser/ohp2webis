#!/usr/bin/python

import re
import os.path
import sys
import time
import urllib
from xml.dom import minidom
from HTMLfive2 import *

meta=META(charset="UTF-8")
meta+=META(name="viewport", content="width=device-width, initial-scale=1")

styleSheet  = LINK(rel="stylesheet", href="../lib/jqstyles/jquery-ui-1.11.4-theme-yellow/jquery-ui.css")
styleSheet += LINK(rel="stylesheet", href="../lib/jqstyles/jquery-mobile-1.4.5-theme-yellow/themes/jqMobileyellow.css")
styleSheet += LINK(rel="stylesheet", href="../lib/jqstyles/jquery-mobile-1.4.5-theme-yellow/themes/jquery.mobile.icons.min.css")
styleSheet += LINK(rel="stylesheet", href="../lib/jquery.mobile-1.4.5/jquery.mobile.structure-1.4.5.min.css")
styleSheet += LINK(rel="stylesheet",type="text/css",href="ohp.css")

javaScript = SCRIPT(type="text/javascript",src="../lib/webis.js")
javaScript += SCRIPT(type="text/javascript",src="../lib/jquery-2.1.4.min.js")
javaScript += SCRIPT(type="text/javascript",src="../lib/jquery.ui-1.11.4/jquery-ui.min.js",integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw=",crossorigin="anonymous")
javaScript += SCRIPT(type="text/javascript",src="ohp.js")
javaScript += SCRIPT(type="text/javascript",src="../lib/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js")
javaScript += SCRIPT(type="text/javascript",src="../lib/JsRootDev/scripts/JSRootCore.min.js?2d&more2d&3d&onload=start_jsroot") # Library for JsRoot, onload here so all libraries are loaded before executing the start up

histViewer='../utils/histo_js.html?histo='
tableViewer='table.html?table='

default_partition = 'ATLAS'

maxNDivX = 5

pluginLinks = []
divs = []
vars = {}
drawOpts = []

def parseXML(file):
    global baseDir
    if file[0] != '/':
      print "Parsing: ",baseDir + '/' + file    
      text=open(baseDir + '/' + file,'r').read()
    else:
      print "Parsing absolute:",file
      text=open(file,'r').read()
    tIdx=text.find('<?xml')
    if tIdx!=-1:
         text=text[text.find('?>',tIdx)+2:]
    text=text.replace('&quot','&quot;')
    text=text.replace('&quot;;','&quot;')
    dom=minidom.parseString('<full>'+ #dom expects single encompassing tag
                            text+
                            '</full>')
    return dom

def parseFile(file, doms):
    dom = parseXML(file)
    doms.append(dom)
    for include in dom.getElementsByTagName('include'):
        parseFile(include.getAttribute('filename'), doms)
    return doms

def getVal(elem,name,default=None):
    val=elem.getElementsByTagName(name)
    if len(val)==0: return default
    if len(val)>1: 
        print 'Found unexpected number of',name,':',len(val),elem
        # return None
    val = val[0].firstChild.nodeValue
    if val[0] == '"': val = val[1:-1]
    # val=str(val[0].firstChild.nodeValue)[1:-1]
    if not val: return default
    else: return val

def getVars(dom, vars): # appends entries to "vars" dictionary
    for var in dom.getElementsByTagName('variable'):
        vars[var.getAttribute('name')] = var.getAttribute('value')

def resolveVars(text, vars):
    for match in re.finditer('\$\{[^}]*\}|\$\([^)]*\)', text):
        try:
            var = match.group() # ${VARIABLE} or $(VARIABLE)
            name = var[2:-1] # same without dollar sign and braces
            value = vars[name]
            text = text.replace(var, value)
        except KeyError:
            print 'Could not resolve variable:', var
    return text

def getDrawOpts(dom, drawOpts):
    options = dom.getElementsByTagName('globaloptions')
    if options:
        hists = options[0].getElementsByTagName('histogram')
        for hist in hists:
            name = resolveVars(hist.getAttribute('name'), vars)
            drawopt = []
            style = []
            xaxis = []
            yaxis = []
            table = None
            for prop in hist.getElementsByTagName('property'):
                if prop.getAttribute('DRAWOPT'):
                    drawopt.append(prop.getAttribute('DRAWOPT'))
                if prop.getAttribute('STYLE'):
                    style.append(prop.getAttribute('STYLE'))
                if prop.getAttribute('XAXIS'):
                    xaxis.append(prop.getAttribute('XAXIS'))
                if prop.getAttribute('YAXIS'):
                    yaxis.append(prop.getAttribute('YAXIS'))
                if prop.getAttribute('TABLE'):
                    table = prop.getAttribute('TABLE')
            if drawopt or style or xaxis or yaxis or table:
                drawOpts.append((re.compile(name + '$'), ' '.join(drawopt) or None, ' '.join(style) or None, ' '.join(xaxis) or None, ' '.join(yaxis) or None, table or None))

def setDrawOpts(urlOpts, key, property, funcName):
    if property and funcName in property:
        start = property.find(funcName)
        value = property[property.find('(', start) + 1:property.find(')', start)]
        urlOpts[key] = urllib.quote(value)

def parseTabs(plugin, pluginNameList):

    tabs = plugin.getElementsByTagName('tabs')
    if not tabs: 
        print 'Found unexpected number of tabs - skipping'
        return -1

    tabs = plugin.getElementsByTagName('tabs')[0].childNodes[0].nodeValue.replace('\n',' ')
    if tabs[0] == '"': tabs = tabs[1:-1]

    sep  = plugin.getElementsByTagName('tabs')[0].getAttribute('token')
    if not sep: sep = ' '

    tabList= [ elem.strip() for elem in tabs.split(sep) if elem != '' ]
    tabLinks=[]

    for tab in tabList:
        pad=plugin.getElementsByTagName(tab)
        if len(pad)==0:
            print 'Found unexpected number of pads: %d skipping' % len(pad),tab
            continue

        pluginNameList.append(tab)

        subtabs = pad[0].getElementsByTagName('tabs')

        if subtabs:
            #id=addStringList("_", pluginNameList)

            tab_menu = parseTabs(pad[0],pluginNameList)

            # add to tabs
            if tab_menu == -1: 
                print 'Found none of the expected subtabs for: %s skipping' % addStringList(" ",pluginNameList),tab
                pluginNameList.pop()
                continue
            else:
                tabLinks.append(LI(H3(tab)+
                                   tab_menu,
                                data_role="collapsible", data_inset="false", data_collapsed_icon="carat-d", data_expanded_icon="carat-u", data_iconpos="right"))

        else:
            if not getVal(pad[0],"ndivx",1): print tab
            ndivx=int(getVal(pad[0],"ndivx",maxNDivX))
            if ndivx > maxNDivX:
                print 'For readability the number of graphs in one row is limited to {0} instead of requested {1} for {2}.'.format(maxNDivX, ndivx, tab)
                ndivx = maxNDivX
            
            if pad[0].getElementsByTagName('histos'):
                histos_name = 'histos'
            elif pad[0].getElementsByTagName('layer1'):
                histos_name = 'layer1'
            else:
                continue
            sep = pad[0].getElementsByTagName(histos_name)[0].getAttribute('token')
            if sep == '': sep = ' '
            histos=[ h.strip() for h in getVal(pad[0],histos_name,'').split(sep) if h != '']
            if len(histos) < ndivx:
                ndivx = len(histos)

            gridList=[]
            histCount = 0
            idPage=addStringList("_",pluginNameList)

            for hist in histos:
                histCount += 1
                if hist == "": continue 
                hist = resolveVars(hist, vars)
                link=hist
                buttonText=link.rsplit('/', 1)[1]
                link=link.replace('/','.',1)

                if link.count('/') == 1:
                    link=link.replace('/','.',1)
                else:
                    link=link.replace('/','./',1)
                (server, provider, histName)=link.split('.',2)
                link="/info/current/"+partition+"/oh/"+link
                tableLink=None
                urlOpts = {}
                for opt in drawOpts:
                    if opt[0].match(hist):
                        if opt[1]: urlOpts['draw'] = urllib.quote(opt[1])
                        setDrawOpts(urlOpts, 'logx',    opt[2], 'SetOptLogx')
                        setDrawOpts(urlOpts, 'logy',    opt[2], 'SetOptLogy')
                        setDrawOpts(urlOpts, 'logz',    opt[2], 'SetOptLogz')
                        setDrawOpts(urlOpts, 'optstat', opt[2], 'SetOptStat')
                        setDrawOpts(urlOpts, 'rangex',  opt[3], 'SetRangeUser')
                        setDrawOpts(urlOpts, 'rangey',  opt[4], 'SetRangeUser')
                        if opt[5]: tableLink = opt[5]
                hrefname = link #Extra for json type option
                if urlOpts:
                    link += '?' + '&'.join('%s=%s' % (k, v) for (k, v) in urlOpts.iteritems())
                    hrefname = link + '&type=json' # Extension for JSROOT integration
                else:
                    hrefname += '?type=json'
                
                gridBlock = "ui-block-" + getGridBlockId(histCount, ndivx)
                
                if tableLink:
                    extra=''
                    if tableLink=='YX':
                        extra='&yx=1'
                    if tableLink=='STAT':
                        extra='&stat=1'
                    plot=DIV(DIV(id=idPage+"_"+str(histCount), Class="plot", data_ohp_graph_address=hrefname, data_ohp_graph_number=histCount)+
                             A(buttonText, id=idPage+"_"+str(histCount)+"_btn", href=tableViewer+urllib.quote(histName+extra), target="_blank", Class="ui-btn ui-icon-eye ui-btn-icon-right ui-mini popup-btn", data_ohp_graph_id=idPage+"_"+str(histCount), data_ohp_func="popup"),
                         Class="plot-grid-item " + gridBlock)
                else:
                    plot=DIV(DIV(id=idPage+"_"+str(histCount), Class="plot", data_ohp_graph_address=hrefname, data_ohp_graph_number=histCount)+
                             A(buttonText, id=idPage+"_"+str(histCount)+"_btn", href=histViewer+urllib.quote(hrefname), target="_blank", Class="ui-btn ui-icon-eye ui-btn-icon-right ui-mini popup-btn", data_ohp_graph_id=idPage+"_"+str(histCount), data_ohp_func="popup"), 
                         Class="plot-grid-item " + gridBlock)
                # Javascript ohp.js depends on: a id = div id+"_btn", and div is before a
                gridList.append(plot)
      
            namePageHeader=addStringList(": ",pluginNameList)
            grid=DIV(Sum(gridList), Class="plot-grid ui-grid-" + getGridBlockId(ndivx-1, ndivx) + " max-grid-" + getGridBlockId(ndivx-1, ndivx))

            div=DIV(DIV(H1(namePageHeader), Class="page-header", data_role="header", data_position="fixed", data_tap_toggle="false")+
                    DIV(grid, role="main", Class="main-content ui-content"),
                data_role="page", id="page_"+idPage, Class="main-page", data_ohp_graph_number_min=1, data_ohp_graph_number_max=len(histos))
            
            divs.append(div)
            tabLinks.append(LI(A(tab, href="#page_"+idPage, Class="ui-btn")))

        pluginNameList.pop()

    #now make plugin div -> menu
    if not tabLinks:
        print 'Found zero (0) tab for: %s skipping' % pluginNameList[0]
        return -1
    else:
        submenu=UL(Sum(tabLinks), data_role="listview")
        return submenu
        
def getGridBlockId(histNumber, maxGrid):
    if maxGrid == 1:
        return "no-grid"
    gridBlockId = ""
    if histNumber % maxGrid == 0:
        if maxGrid == 2:
            gridBlockId = "b"
        elif maxGrid == 3:
            gridBlockId = "c"
        elif maxGrid == 4:
            gridBlockId = "d"
        elif maxGrid == 5:
            gridBlockId = "e"
    elif histNumber % maxGrid == 1:
        gridBlockId = "a"
    elif histNumber % maxGrid == 2:
        gridBlockId = "b"
    elif histNumber % maxGrid == 3:
        gridBlockId = "c"
    elif histNumber % maxGrid == 4:
        gridBlockId = "d"
    return gridBlockId

def addStringList(seperator, stringList):
    def sepReduce(a, b):
        return a + seperator + b
    return reduce(sepReduce, stringList)


baseDir = os.path.dirname(sys.argv[1])
title   = sys.argv[2]
if len(sys.argv) > 3: default_partition = sys.argv[3]
inputs = parseFile(os.path.basename(sys.argv[1]), [])


for dom in inputs:
    getVars(dom, vars)

for dom in inputs:
    getDrawOpts(dom, drawOpts)

for dom in inputs:
    # default partition name
    partition=default_partition
    partitions=dom.getElementsByTagName('partition')
    if len(partitions)!=0:
        partition=partitions[0].getAttribute('name')

    # plugins
    for plugin in dom.getElementsByTagName('plugin'):

        # supported plugins that we understand
        if plugin.getAttribute('type') in [ 'MDIPlugin', 'StatusPlugin', 'LegacyButtonsPlugin', 'BrowserPlugin', 'RunStatusPlugin' ]: continue;
        if not plugin.getAttribute('type') in [ 'HistoWindowPlugin', 'HistoWindowTabPlugin', 'LArHistoWindowTabPlugin', 'HistoWindowRPPlugin', 'HistogramRootPlugin' ]:
            print 'Unknown plugin:',plugin.getAttribute('type')
            continue
        
        pluginName=plugin.getAttribute('name')
        tab_menu = parseTabs(plugin, [pluginName])
        if tab_menu == -1: 
            pluginLinks.append(LI(A(pluginName, href="#page_"+pluginName, Class="ui-btn")))
        else :
            pluginLinks.append(LI(H3(pluginName)+
                                  tab_menu,
                               data_role="collapsible", data_inset="false", data_collapsed_icon="carat-d", data_expanded_icon="carat-u", data_iconpos="right"))

extras=[
        ("Rates","../trigger/rates.html"),
        ("WTRP","../../../../wmi/current/WTRP_wmi/"),
        ("Browser","../app/oh.html?release=current&partition="+partition),
        ]

#for extra in extras:
#    pluginLinks.append(LI(A(extra[0],
#                            href=extra[1])))

#integration of the extras into the page structure
for extra in extras:
    div=DIV(DIV(H1(extra[0]),
            Class="page-header", data_role="header", data_position="fixed", data_tap_toggle="false")+
            DIV(IFRAME(src="", title=extra[1], Class="ext-html"),
            role="main", Class="main-content ui-content"),
        data_role="page", id="page_"+extra[0], Class="main-page")
    divs.append(div)
    pluginLinks.append(LI(A(extra[0], href="#page_"+extra[0], Class="ui-btn")))

externalPopup=DIV(A("Close", href="#", data_rel="back", Class="ui-btn ui-corner-all ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right")+
                  DIV(id="graph-popup-drawing", Class="popup-drawing", data_ohp_graph_address="", data_ohp_graph_id="", data_ohp_graph_number=0, data_ohp_graph_number_min=1, data_ohp_graph_number_max=1)+
                  DIV(A("Prev", Class="ui-btn popup-prev-btn", href="", id="popup-prev")+
                      A("Man. refresh", id="refresh-btn-popup", Class="ui-btn ui-icon-refresh ui-btn-icon-right refresh-button")+
                      A("Next", Class="ui-btn popup-next-btn", href="", id="popup-next"),
                  data_role="controlgroup", data_type="horizontal"),
              data_role="popup", id="graph-popup", Class="external-popup", data_theme="a")

externalHeader=DIV(A("Menu", href="#menuPanel", Class="ui-btn ui-icon-bars ui-btn-icon-notext ui-corner-all menu-button")+
                   H1(TEXT(partition+":")+SPAN(A("Login", href="/info/current/"+partition+"/oh"), title="status", id="status"))+
                   A("Load all", Class="ui-btn ui-icon-refresh ui-btn-icon-right load-all-button"),
               id="curpartition", Class="main-header", data_role="header", data_position="fixed", data_theme="a", data_tap_toggle="false", data_ohp_partition=partition)

externalFooter=DIV(H3(DIV(DIV(LABEL("time [s]",For="timedelay", id="label-timedelay")+
                              INPUT(Class="input-timedelay", data_mini="true", name="timedelay", id="timedelay", value="60", type="number"),
                          Class="ui-block-a footer-grid-input")+
                          DIV(A("Auto refresh", Class="ui-btn ui-mini ui-icon-recycle ui-btn-icon-right auto-up-btn", data_ohp_func="start"),
                          Class="ui-block-b footer-grid-btn")+
                          DIV(A("Man. refresh", Class="ui-btn ui-icon-refresh ui-btn-icon-right ui-mini refresh-button"),
                          Class="ui-block-c footer-grid-btn"),
                      Class="ui-grid-b"),
                   Class="ui-bar"),
               Class="main-footer", data_role="footer", data_position="fixed", data_theme="a", data_tap_toggle="false")

startpage=DIV(DIV(H1("Startpage: "+partition), Class="page-header", data_role="header", data_position="fixed", data_tap_toggle="false")+
              DIV(H1("Startpage: "+partition)+
                  P("Some settings to choose, you need to submit them!")+
                  DIV(LABEL("Zooming with mouse wheel (click and touch still enabled)", For="flip-zooming")+
                      SELECT(OPTION("Off", value="off")+
                             OPTION("On", value="on"),
                      name="flip-zooming", id="flip-zooming", data_role="slider", data_mini="true")+
                      LABEL("Allow loading on small screens", For="flip-allow-loading")+
                      SELECT(OPTION("No", value="off")+
                             OPTION("Yes", value="on"),
                      name="flip-allow-loading", id="flip-allow-loading", data_role="slider", data_mini="true")+
                      LABEL("Colour palette", For="input-palette")+
                      INPUT(name="input-palette", id="input-palette", value="30", min="1", max="112", step="1", type="range", data_highlight="true")+
                      A("Submit", Class="ui-btn ui-mini submit-set-btn"),
                  Class="startpage-options"),
              role="main", Class="main-content ui-content"),
          data_role="page", id="startpage", Class="main-page")

menuStartpage=LI(A(partition, href="#startpage", Class="ui-btn ui-icon-home ui-btn-icon-right"))

panelMenu=DIV(UL(menuStartpage+Sum(pluginLinks), data_role="listview"),
          data_role="panel", id="menuPanel", Class="menu-panel", data_position="left", data_position_fixed="true", data_display="overlay", data_theme="a")


html=HTMLfive(HEAD(TITLE(title)+
                   meta+
                   styleSheet+
                   javaScript)+
              BODY(externalHeader+
                   startpage+
                   Sum(divs)+
                   externalFooter+
                   panelMenu+
                   externalPopup))

filename=title + '.html'
fh=open(filename,'w')
fh.write(str(html))
fh.close()
